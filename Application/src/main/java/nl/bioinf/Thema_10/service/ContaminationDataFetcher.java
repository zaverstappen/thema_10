package nl.bioinf.Thema_10.service;

import nl.bioinf.Thema_10.model.FarmMeasurement;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 * Reading the data from the farms and returning
 * it in the JSON format
 */
public class ContaminationDataFetcher {

    /**
     * read the data file and transform
     * it into JSON data
     * @param file path to the data file
     * @return the data in JSON format
     * @throws ParseException
     */
    public String responseToJSON(String file) throws ParseException {
        Path path = Paths.get(file);
        List<FarmMeasurement> data = new ArrayList<>();

        //read the file
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            int lineNumber = 0;
            ArrayList<String> lines = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                //skip the header line
                if (lineNumber == 1) continue;
                if (lines.contains(line)) continue;
                lines.add(line);
                // make a list of the elements in the line
                String[] elements = line.split(";");

                FarmMeasurement farmMeasurement = new FarmMeasurement(elements[0],
                                                                      elements[1],
                                                                      elements[2],
                                                                      elements[3],
                                                                      elements[4]);
                data.add(farmMeasurement);
            }
        } catch (IOException e) {
                e.printStackTrace();
            }

        // return the data in JSON format
        return new Gson().toJson(data);
    }
}
