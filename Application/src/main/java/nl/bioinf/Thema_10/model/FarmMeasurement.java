package nl.bioinf.Thema_10.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * class that stores the information
 * about the measurement of presence
 * of contamination at a farm
 */
public class FarmMeasurement {
    int farmID;
    double latitude;
    double longitude;
    Date date;
    boolean contamination;

    /**
     * constructor
     * @param farmID the id for the farm
     * @param latitude the latitude of the location of the farm
     * @param longitude the longitude of the location of the farm
     * @param date the data of the measurement
     * @param contamination boolean indicating presence of contamination
     * @throws ParseException
     */
    public FarmMeasurement(String farmID, String latitude, String longitude, String date, String contamination) throws ParseException {
        setFarmID(farmID);
        setLatitude(latitude);
        setLongitude(longitude);
        setDate(date);
        setContamination(contamination);
    }

    public int getFarmID() {
        return farmID;
    }

    public void setFarmID(String farmID) {
        this.farmID = Integer.parseInt(farmID);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = Float.parseFloat(latitude);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = Float.parseFloat(longitude);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {
        this.date = new SimpleDateFormat("dd/MM/yyyy").parse(date);
    }

    public boolean isContamination() {
        return contamination;
    }

    public void setContamination(String contamination) {
        this.contamination = Boolean.parseBoolean(contamination);
    }

    @Override
    public String toString() {
        return "Contamination{" +
                "farmID=" + farmID +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", date=" + date +
                ", contamination=" + contamination +
                '}';
    }


}
