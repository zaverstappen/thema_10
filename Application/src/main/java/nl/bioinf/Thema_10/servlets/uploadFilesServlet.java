package nl.bioinf.Thema_10.servlets;

import nl.bioinf.Thema_10.config.WebConfig;
import nl.bioinf.Thema_10.service.NewDataParser;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

@WebServlet(name = "uploadFilesServlet", urlPatterns = "/uploadFiles", loadOnStartup = 1)
public class uploadFilesServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private String uploadDir;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        this.uploadDir = getInitParameter("upload_dir");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        templateEngine.process("uploadFilesPage", ctx, response.getWriter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        // Make new file and save in given directory
        File fileSaveDir = new File(this.uploadDir);
        if (! fileSaveDir.exists()) {
            throw new IllegalStateException("Upload dir does not exist: " + this.uploadDir);
        }
        File generatedFile = new File("my_upload.csv");

        String uploadFileName;

        // Read uploaded file
        for (Part part : request.getParts()) {
            String extension = "";

            // Get name of uploaded file
            uploadFileName = part.getSubmittedFileName();

            // Fetch extension of uploaded file
            int i = uploadFileName.lastIndexOf('.');
            if (i >= 0) {
                extension = uploadFileName.substring(i+1);
            }
            // Check if extension of uploaded file is csv of txt
            if (extension.equals("txt") || extension.equals("csv")) {
                // Write content of uploaded file to newly made file
                part.write(this.uploadDir + File.separator + generatedFile.getName());

                // Get file paths
                String newData = getServletContext().getInitParameter("new_data");
                String originalData = getServletContext().getInitParameter("contamination_data");

                // Execute class for writing new lines to original data file
                NewDataParser dataParser = new NewDataParser();
                dataParser.start(newData, originalData);
                // Renders map page if extensions is correct
                templateEngine.process("map", ctx, response.getWriter());
            } else {
                // Renders an error page when the file has the incorrect extension
                templateEngine.process("extensionError", ctx, response.getWriter());
            }
        }
        }
    }

