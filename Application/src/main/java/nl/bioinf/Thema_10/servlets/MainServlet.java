package nl.bioinf.Thema_10.servlets;

import nl.bioinf.Thema_10.config.WebConfig;
import nl.bioinf.Thema_10.service.ContaminationDataFetcher;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

@WebServlet(name = "MainServlet", urlPatterns = "/home", loadOnStartup = 1)
public class MainServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = this.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        templateEngine.process("homepage", ctx, response.getWriter());
    }

}