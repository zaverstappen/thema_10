package nl.bioinf.Thema_10.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Reading the newly saved uploaded file
 * and write the content to the original data file
 */
public class NewDataParser {

    /**
     * Executing the writeFile function with the given data paths and
     * after writing the content to another file, the uploaded file is deleted.
     * @param newData path to the saved uploaded file
     * @param originalData path to the original data file
     */
    public void start(String newData, String originalData){
        // Executes writeFile function
        NewDataParser dataParser = new NewDataParser();
        dataParser.writeFile(newData, originalData);

        // Get path to uploaded data
        Path newDataPath = Paths.get(newData);

        // Delete uploaded file if exists
        try {
            Files.deleteIfExists(newDataPath);
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    /**
     * Reading the lines of the uploaded file and writing them to the original data file
     * @param newFile path to the saved uploaded file
     * @param originalFile path to the original data
     */
    private void writeFile(String newFile, String originalFile) {
        // Read lines of uploaded file
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(newFile))) {
            String line;
            int lineNumber = 0;

            // Writer every line to the original file
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                if (lineNumber == 1) continue;
                try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(originalFile), StandardOpenOption.APPEND)) {
                    writer.newLine();
                    writer.write(line);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
