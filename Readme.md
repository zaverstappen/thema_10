# Animation of salmonella spread

Names: Carlijn Ruijter & Zoë Verstappen  
Date: 31-01-2020

### Requirements
- Install tomcat (see https://tomcat.apache.org/download-90.cgi)
- Configure tomcat as webserver in IntelliJ (see https://www.jetbrains.com/help/idea/configuring-and-managing-application-server-integration.html)
- The application needs to be opened in Firefox or Google Chrome

### Installation

- Clone the project
- Open the project in IntelliJ
- Edit '...' in the web.xml file with the correct paths
  - The parameter 'contamination_data' must contain the path to the original data file
  - The parameter 'upload_dir' must be the path to the directory where the uploaded file needs to be placed.
  - The location in the multipart-config needs to be the same path as the 'upload_dir' path.
- Add a run configuration
  - The URL must end with '/home'

Run the configuration to launch the web application 

### Data format
The used data needs to be a csv or txt file. The file must contain a header with the columns:  
farmId, latitude, longitude, date, contamination

### Usage

Information about the usage of the application is stored on the home page.

### Information
Information about the java classes is given per file.  
  
##### -FarmMeasurment.java:
Class that makes an instance of one measurement of a farm.  
  
##### -ContaminationDataFetcher:
Class for parsing the data to objects and transforming it to JSON format.  
  
##### -NewDataParser:
Class for writing new uploaded data to the original data file.
