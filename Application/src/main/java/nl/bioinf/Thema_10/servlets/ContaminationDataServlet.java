package nl.bioinf.Thema_10.servlets;

import nl.bioinf.Thema_10.service.ContaminationDataFetcher;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

@WebServlet(name = "ContaminationDataServlet", urlPatterns = "/mapData")
public class ContaminationDataServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // get the location of the data file
        String file = getServletContext().getInitParameter("contamination_data");

        // get the data in the json format
        ContaminationDataFetcher fetcher = new ContaminationDataFetcher();
        String json = null;
        try {
            json = fetcher.responseToJSON(file);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
